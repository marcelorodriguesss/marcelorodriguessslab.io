=================
Rsync
=================

------------------------------------------------
How preserve directory tree while copying with rsync?
------------------------------------------------

.. code-block:: bash

    # ref: https://tinyurl.com/38hl3wag

    rsync -av -m --include '*/' --include '*_en51_*' --exclude '*' ./folder1 /folder2
