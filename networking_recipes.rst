===============
Networking
===============

--------------------------------
How to Disable IPv6 on RHEL/CentOS 8
--------------------------------

* ref: https://linoxide.com/disable-ipv6-on-centos-8/

*  Before we disable the IPv6 protocol, let us first do a check to see what the value of the command net.ipv6.conf.default.disable_ipv6 is. You should get the value of 0.

.. code-block:: shell

    sudo sysctl \
         --values \
         net.ipv6.conf.default.disable_ipv6

* Create a new configuration file as shown:

.. code-block:: shell

    sudo cat << EOF | sudo tee /etc/sysctl.d/70-ipv6.conf
    net.ipv6.conf.all.disable_ipv6 = 1
    net.ipv6.conf.default.disable_ipv6 = 1
    EOF

* To disable IPv6, execute the command:

.. code-block:: shell

    sudo sysctl \
         --load \
         /etc/sysctl.d/70-ipv6.conf

* Now, verify the sysctl value once more and this time, you will get the value of 1 indicating that Ipv6 has been disabled.

.. code-block:: shell

    sudo sysctl \
         --values \
         net.ipv6.conf.default.disable_ipv6
