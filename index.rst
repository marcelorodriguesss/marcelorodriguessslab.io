.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Home
====

Python
===============

.. toctree::
   :maxdepth: 2

   python_recipes
.. python_packages
.. python_install

Numpy
==============

.. toctree::
   :maxdepth: 2

   numpy_recipes

.. Xarray
.. ===============

.. toctree::
.. :maxdepth: 2

.. xarray_recipes

.. Pandas
.. ===============

.. .. toctree::
..    :maxdepth: 2

..    pandas_recipes

.. Matplotlib
.. ================

.. Basemap
.. ================

.. Cartopy
.. ================

Python Dates
================

.. toctree::
   :maxdepth: 2

   dates_recipes

Bash
================

.. toctree::
   :maxdepth: 2

   bash_recipes

Debian Linux
================

.. toctree::
   :maxdepth: 2

   debian_packages

Rsync
================

.. toctree::
   :maxdepth: 2

   rsync_recipes

LFTP
================

.. toctree::
   :maxdepth: 2

   lftp_recipes

Docker
================

.. toctree::
   :maxdepth: 2

   docker_recipes

Kubernetes
================

.. toctree::
   :maxdepth: 2

   kubernetes_recipes

Networking
================

.. toctree::
   :maxdepth: 2

   networking_recipes

Numerical Models
================

.. toctree::
   :maxdepth: 2

   num_model_recipes
