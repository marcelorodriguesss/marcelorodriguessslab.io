=================
Python Dates
=================

------------------------------------------------
How do I calculate number of days between two dates?
------------------------------------------------

.. code-block:: python

    # ref: https://bit.ly/2XKOIVj

    from datetime import date
    d0 = date(2017, 8, 18)
    d1 = date(2017, 10, 26)
    delta = d1 - d0
    print(delta.days)
