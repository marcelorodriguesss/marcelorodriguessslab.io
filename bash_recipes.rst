=================
Bash
=================

------------------------------------------------
How to reverse loop Shell Script?
------------------------------------------------

.. code-block:: bash

    for I in {2021..1981..-1} ; do
        echo $I
    done

------------------------------------------------
How to change permissions using find command?
------------------------------------------------

.. code-block:: bash

    # files
    find .  -type f -print0 | xargs -0 chmod 644

.. code-block:: bash

    # folders
    find .  -type d -print0 | xargs -0 chmod 755
