=================
Numerical Models
=================

------------------------------------------------
Compile WRF4
------------------------------------------------

.. code-block:: shell

    vim ${HOME}/.bashrc

    ### WRF ###
    export WRFDIR="${HOME}/Build_WRF/LIBRARIES"
    export CC="gcc"
    export CXX="g++"
    export FC="gfortran"
    export FCFLAGS="-m64"
    export F77="gfortran"
    export FFLAGS="-m64"
    export JASPERLIB="${WRFDIR}/grib2/lib"
    export JASPERINC="${WRFDIR}/grib2/include"
    export LDFLAGS="-L${WRFDIR}/grib2/lib"
    export CPPFLAGS="-I${WRFDIR}/grib2/include"
    export NETCDF=${WRFDIR}/netcdf
    export PATH=${WRFDIR}/netcdf/bin:${WRFDIR}/mpich/bin:$PATH
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${WRFDIR}/grib2/lib:${WRFDIR}/hdf5/lib:${WRFDIR}/netcdf4/lib

    # # # # #

    mkdir -pv ${HOME}/Build_WRF/LIBRARIES

    # compilers tests 
    
    cd ${HOME}/Build_WRF
    
    mkdir Fortran_C_tests
    
    cd Fortran_C_tests
    
    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_tests.tar
    
    tar -xf Fortran_C_tests.tar
    
    gfortran TEST_1_fortran_only_fixed.f
    ./a.out
    
    gfortran TEST_2_fortran_only_free.f90
    ./a.out
    
    gcc TEST_3_c_only.c
    ./a.out
    
    gcc -c -m64 TEST_4_fortran+c_c.c
    gfortran -c -m64 TEST_4_fortran+c_f.f90
    ./a.out

    # netcdf
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-4.1.3.tar.gz
    
    tar xzvf netcdf-4.1.3.tar.gz
    
    cd netcdf-4.1.3
    
    ./configure --prefix=${WRFDIR}/netcdf \
	    --disable-dap \
	    --disable-netcdf-4 \
	    --disable-shared
    
    make && make install

    # mpich
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/mpich-3.0.4.tar.gz
    
    tar xzvf mpich-3.0.4.tar.gz
    
    cd mpich-3.0.4
    
    ./configure --prefix=${WRFDIR}/mpich
    
    make && make install

    # zlib
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/zlib-1.2.7.tar.gz
    
    tar xzvf zlib-1.2.7.tar.gz
    
    cd zlib-1.2.7
    
    ./configure --prefix=${WRFDIR}/grib2
    
    make && make install

    # libpng
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/libpng-1.2.50.tar.gz
    
    tar xzvf libpng-1.2.50.tar.gz
    
    cd libpng-1.2.50
    
    ./configure --prefix=${WRFDIR}/grib2
    
    make && make install

    # jasper
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/jasper-1.900.1.tar.gz
    
    tar xzvf jasper-1.900.1.tar.gz
    
    cd jasper-1.900.1
    
    ./configure --prefix=${WRFDIR/}grib2
    
    make && make install

    # mpi tests

    cd ${HOME}/Build_WRF

    mkdir Fortran_C_NETCDF_MPI_tests

    cd Fortran_C_NETCDF_MPI_tests

    wget http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_NETCDF_MPI_tests.tar

    tar -xf wget Fortran_C_NETCDF_MPI_tests.tar
    
    cp -v ${NETCDF}/include/netcdf.inc .
    
    gfortran -c 01_fortran+c+netcdf_f.f
    
    gcc -c 01_fortran+c+netcdf_c.c
    
    gfortran 01_fortran+c+netcdf_f.o \
        01_fortran+c+netcdf_c.o \
        -L${NETCDF}/lib \
        -lnetcdff \
        -lnetcdf
    
    ./a.out

    mpif90 -c 02_fortran+c+netcdf+mpi_f.f
    
    mpicc -c 02_fortran+c+netcdf+mpi_c.c
    
    mpif90 02_fortran+c+netcdf+mpi_f.o \
        02_fortran+c+netcdf+mpi_c.o \
        -L${NETCDF}/lib \
        -lnetcdff \
        -lnetcdf
    
    mpirun ./a.out

    # WRF
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/src/WRFV4.0.TAR.gz
    
    tar -xzvf WRFV4.0.TAR.gz
    
    cd WRF
    
    ./configure
    
    ./compile em_real
    
    ls -ltr main/*.exe

    # WPS
    
    cd ${HOME}/Build_WRF
    
    wget http://www2.mmm.ucar.edu/wrf/src/WPSV4.0.TAR.gz
    
    tar -xzvf WPSV4.0.TAR.gz
    
    cd WPS
    
    ./clean
    
    ./configure
    
    ./compile
    
    ls -ltr
