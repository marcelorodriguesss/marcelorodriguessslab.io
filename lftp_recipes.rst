===============
LFTP
===============

--------------------------------
Mirror files/dirs wildcards
--------------------------------

.. code-block:: shell

    # upload
    $ mirror -R --include-glob=*.csv

    # download
    $ mirror --exclude=logs/
