=============
Numpy
=============

------------------------------------------------
How to compute array moving average?
------------------------------------------------

.. code-block:: python

    import numpy as np
    
    def compute_mov_ave(arr3d, window=3):
        '''
        arr3d: numpy -> time monthly x lat x lon
        window = 3  -> 3 months
        '''
        arr_ma = np.empty((arr.shape[0] - window + 1, arr.shape[1], arr.shape[2]))
        w = np.ones(window) / window  # weights
        for y in range(arr.shape[1]):
            for x in range(arr.shape[2]):
                arr_ma[:, y, x] = np.convolve(arr[:, y, x], w, mode='valid')
        return arr_ma
