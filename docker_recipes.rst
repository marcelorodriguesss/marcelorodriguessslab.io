=================
Docker
=================

------------------------------------------------
How to clear Docker container logs
------------------------------------------------

.. code-block:: bash

    for container_id in $(docker ps -a | grep Up | awk '{print $1}')
    do
        echo "" >  $(docker inspect --format='{{.LogPath}}' ${container_id})
    done

------------------------------------------------
How to remove all Exited containers
------------------------------------------------

.. code-block:: bash

    docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs docker rm

