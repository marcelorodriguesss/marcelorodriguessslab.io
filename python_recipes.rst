=================
Python
=================

------------------------------------------------
How to compile Python from source code?
------------------------------------------------

Tested on Linux Debian 10

Install Dependencies:

.. code-block:: bash

    sudo apt update

.. code-block:: bash

    sudo apt install make build-essential libssl-dev zlib1g-dev \
        libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
        libncurses5-dev libncursesw5-dev xz-utils tk-dev \
        libffi-dev libexpat1-dev libcap-dev \
        libgdbm-dev liblzma-dev uuid-dev \
        dvipng texlive-latex-base libgdbm-compat-dev \
        libgeos-dev libgeos-c1v5 libgeos-3.7.1

* Download Python3.6 source code:

.. code-block:: bash

    VERSION="3.6.12"
    INSTALL_DIR="${HOME}/Python3"

.. code-block:: bash

    cd $HOME
    wget -qO- https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tar.xz | tar Jxv

* Compiling Python3.6 source code:

.. code-block:: bash

    cd $HOME/Python-${VERSION}
    mkdir ${INSTALL_DIR}
    mkdir ${INSTALL_DIR}/lib

.. code-block:: bash

    ./configure --prefix=${INSTALL_DIR} \
        --enable-shared \
        LDFLAGS="-Wl,-rpath ${INSTALL_DIR}/lib" \
        --with-system-expat \
        --with-system-ffi \
        --with-ensurepip=yes
        # --enable-optimizations

.. code-block:: bash

    make -j `nproc`

.. code-block:: bash

    make altinstall

* After install, add Python to PATH:

.. code-block:: bash

    PATH="${HOME}/Python3/bin:$PATH"
    export PATH
    LD_LIBRARY_PATH="${HOME}/Python3/lib:$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH

* and open a new terminal and upgrade Python Pip package:

.. code-block:: bash

    python3.6 -m pip install --upgrade pip

------------------------------------------------
How to install Python packages in Linux?
------------------------------------------------

.. code-block:: bash

    python3.6 -m pip install numpy scipy netcdf4 xarray matplotlib pandas

.. code-block:: bash

    # Shapely

    python3.6 -m pip install shapely --no-binary shapely

.. code-block:: bash

    cd $HOME

    wget -qO- https://github.com/matplotlib/basemap/archive/v1.2.2rel.tar.gz | tar xvz

    cd basemap-1.2.2rel

    python3.6 -m pip install .

------------------------------------------------
How to install Python packages in Windows?
------------------------------------------------

.. code-block:: bash

    python3.6 -m pip install wheel

.. code-block:: bash

    # https://stackoverflow.com/a/60651842/1236254
    # list of compatible tags
    python3.6 -m pip debug --verbose

.. code-block:: bash

    # Unofficial Windows Binaries for Python Extension Packages
    # https://www.lfd.uci.edu/~gohlke/pythonlibs/
    python3.6 -m pip install pyproj‑1.9.5.1‑cp36‑cp36m‑win32.whl
    python3.6 -m pip install basemap‑1.1.0‑cp36‑cp36m‑win32.whl

------------------------------------------------
How can I safely create a nested directory?
------------------------------------------------

.. code-block:: python

    # ref: https://stackoverflow.com/a/273227

    # Ex1
    os.makedirs("/my/directory", exist_ok=True)

    # Ex2
    # Python >= 3.5
    from pathlib import Path
    Path("/my/directory").mkdir(parents=True, exist_ok=True)

    # Ex3
    # older versions of Python
    import os
    if not os.path.exists("/my/directory"):
        os.makedirs("/my/directory")

------------------------------------------------
How to write a list to txt file?
------------------------------------------------

.. code-block:: python

    # ref:

    # Python >= 3.6
    with open('myfile.txt', 'w') as fout:
        print(*mylist, sep='\n', file=fout)

------------------------------------------------
How to change working directory to script directory?
------------------------------------------------

.. code-block:: python

    import os

    abspath = os.path.abspath(__file__)
    root_dir = os.path.dirname(abspath)
    os.chdir(root_dir)
