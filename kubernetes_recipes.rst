=================
Kubernetes
=================

------------------------------------------------
The kubectl scale command lets your instantaneously change the number of replicas you want to run your application.
------------------------------------------------

.. code-block:: bash

    kubectl scale deployment <pod> --replicas <value>

